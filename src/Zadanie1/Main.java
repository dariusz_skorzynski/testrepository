package Zadanie1;

import java.io.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        File plik = new File("form.txt");
        Scanner scanner = new Scanner(System.in);
        boolean isWorking = true;
        try (PrintWriter writer = new PrintWriter(new FileWriter("form.txt", true))) {


            while (isWorking) {

                System.out.println("Podaj imie i nazwisko");
                String zmienna = scanner.nextLine();

                if (zmienna.equals("quit")) break;

//                writer.println(zmienna);
                System.out.println("Podaj wiek");
                String word = scanner.nextLine();
                int wiek = Integer.parseInt(word);
//                writer.println(wiek);
                System.out.println("Podaj płeć");
                String plec = scanner.nextLine();
//                writer.println(plec);
                String odpowiedz1 = null;
                String odpowiedz2 = null;
                String odpowiedz3 = null;

                if (plec.equals("kobieta") && (wiek >= 18 && wiek <= 25)) {
                    System.out.println("Kolejne pytanie1");
                    odpowiedz1 = scanner.nextLine();
//                    writer.println(odpowiedz1);
                    System.out.println("Kolejne pytanie2");
                    odpowiedz2 = scanner.nextLine();
//                    writer.println(odpowiedz2);
                    System.out.println("Kolejne pytanie3");
                    odpowiedz3 = scanner.nextLine();
//                    writer.println(odpowiedz3);
                }
                if (plec.equals("mezczyzna") && (wiek >= 25 && wiek <= 30)) {
                    System.out.println("Kolejne pytanie1");
                    odpowiedz1 = scanner.nextLine();
//                    writer.println(odpowiedz1);
                    System.out.println("Kolejne pytanie2");
                    odpowiedz2 = scanner.nextLine();
//                    writer.println(odpowiedz2);
                    System.out.println("Kolejne pytanie3");
                    odpowiedz3 = scanner.nextLine();
//                    writer.println(odpowiedz3);
                }
                Form form = new Form(zmienna, wiek, plec, odpowiedz1, odpowiedz2, odpowiedz3);
                form.setImie_nazwisko(zmienna);
                form.setWiek(wiek);
                form.setPlec(plec);
//                if (odpowiedz1 == null) {
//                    odpowiedz1 = "Odpowiedź niewymagana";}
                    form.setOdpowiedz1(odpowiedz1);
//
//                if (odpowiedz2 == null) {
//                    odpowiedz2 = "Odpowiedź niewymagana";
//                }
                form.setOdpowiedz2(odpowiedz2);
//                if (odpowiedz3 == null) {
//                    odpowiedz3 = "Odpowiedź niewymagana";
//                }
                form.setOdpowiedz3(odpowiedz3);
                writer.println();
                writer.print(form);
            }

        } catch (FileNotFoundException fnfe) {
            System.out.println("Nie ma takiego pliku.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

