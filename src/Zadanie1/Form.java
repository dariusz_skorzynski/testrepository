package Zadanie1;

public class Form {
    private String imie_nazwisko;
    private int wiek;
    private String plec;
    private String odpowiedz1=null;
    private String odpowiedz2=null;
    private String odpowiedz3=null;

    public void printTofile (){



    }
    @Override
    public String toString() {
        return "Form{" +
                "imie_nazwisko='" + imie_nazwisko + '\'' +
                ", wiek=" + wiek +
                ", plec='" + plec + '\'' +
                ", odpowiedz1='" + odpowiedz1 + '\'' +
                ", odpowiedz2='" + odpowiedz2 + '\'' +
                ", odpowiedz3='" + odpowiedz3 + '\'' +
                '}';
    }

    public void setImie_nazwisko(String imie_nazwisko) {
        this.imie_nazwisko = imie_nazwisko;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public void setPlec(String plec) {
        this.plec = plec;
    }

    public void setOdpowiedz1(String odpowiedz1) {
        this.odpowiedz1 = odpowiedz1;
    }

    public void setOdpowiedz2(String odpowiedz2) {
        this.odpowiedz2 = odpowiedz2;
    }

    public void setOdpowiedz3(String odpowiedz3) {
        this.odpowiedz3 = odpowiedz3;
    }

    public Form(String imie_nazwisko, int wiek, String plec, String odpowiedz1, String odpowiedz2, String odpowiedz3) {

        this.imie_nazwisko = imie_nazwisko;
        this.wiek = wiek;
        this.plec = plec;
        this.odpowiedz1 = odpowiedz1;
        this.odpowiedz2 = odpowiedz2;
        this.odpowiedz3 = odpowiedz3;
    }
}


